package com.example.nejcstebe.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class myAscents extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ascents);

        String result = "";
        Intent callingIntent = getIntent();

        if(callingIntent != null) {
            Bundle receivedBundle = callingIntent.getExtras();
            if(receivedBundle != null) {
                ArrayList<String> list = receivedBundle.getStringArrayList("Vodnik.ascents");

                String[] ascents = list.toArray(new String[list.size()]);

                ListView lv = (ListView) findViewById(R.id.listView2);

                // create adapter from array and fill the ListView
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(lv.getContext(), android.R.layout.simple_list_item_1, ascents);
                lv.setAdapter(adapter);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_ascents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
