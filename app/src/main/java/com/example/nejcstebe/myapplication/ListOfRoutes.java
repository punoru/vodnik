package com.example.nejcstebe.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// If the class is set to extend a ListActivity (like my other activities) then "unfortunately application has stopped" without any exception!?!?!
public class ListOfRoutes extends Activity {
    JSONObject routesDictionary;
    ArrayAdapter<String> adapter;
    String cragName;
    int numberOfRoutes;
    String sectorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_routes);

        String result = "";
        Intent callingIntent = getIntent();

        if(callingIntent != null) {
            Bundle receivedBundle = callingIntent.getExtras();
            if(receivedBundle != null) {
                result = receivedBundle.getString("Vodnik.routesDictionary");
                if(result != null) {

                    try {
                        TextView tvSectorName = (TextView) findViewById(R.id.textViewListOfRoutes);

                        // make a JSONObject containing all the crags, sectors, routes, grades ...
                        routesDictionary = new JSONObject(result);

                        cragName = receivedBundle.getString("Vodnik.cragName");
                        sectorName = receivedBundle.getString("Vodnik.sectorName");

                        tvSectorName.setText("Sektor " + sectorName + ":");

                        JSONObject routesObj = routesDictionary;

                        // JSONObject -> ArrayList -> String[]
                        Iterator<String> iterRoutes = routesObj.keys();
                        List<String> routesList = new ArrayList<String>();

                        while(iterRoutes.hasNext()) {
                            routesList.add(iterRoutes.next());
                        }

                        final String[] routes = routesList.toArray(new String[routesList.size()]);

                        ListView lv = (ListView) findViewById(R.id.listView);

                        // Use custom adapter for list_item_2 which has two textViews
                        adapter = new ArrayAdapter<String>(lv.getContext(), android.R.layout.simple_list_item_2, android.R.id.text1, routes) {
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                                text1.setText(routes[position]);
                                try {
                                    text2.setText(routesDictionary.getJSONObject(routes[position]).getString("grade"));
                                } catch (JSONException e) {
                                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                                return view;
                            }
                        };

                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                String routeName = (String) adapter.getItem(position);

                                try {
                                    String routeGrade = routesDictionary.getJSONObject(routeName).getString("grade");
                                    String routeDesc = routesDictionary.getJSONObject(routeName).getString("description");
                                    int routeLength = routesDictionary.getJSONObject(routeName).getInt("length");

                                    Intent intent = new Intent();
                                    intent.setClass(getApplicationContext(), RouteDetails.class);
                                    Bundle bundle = new Bundle();

                                    bundle.putString("Vodnik.routeName", routeName);
                                    bundle.putString("Vodnik.routeGrade", routeGrade);
                                    bundle.putString("Vodnik.routeDesc", routeDesc);
                                    bundle.putInt("Vodnik.routeLength", routeLength);

                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                } catch (JSONException e) {

                                }
                            }
                        });

                    } catch (JSONException e) {
                        Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_of_routes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
