package com.example.nejcstebe.myapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ListOfCrags extends ListActivity {
    ArrayAdapter<String> adapter = null;
    JSONObject dictionary = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_crags);

        String result = "";
        Intent callingIntent = getIntent();

        if(callingIntent != null) {
            Bundle receivedBundle = callingIntent.getExtras();
            if(receivedBundle != null) {
                result = receivedBundle.getString("Vodnik.dictionary");
                if(result != null) {
                    try {
                        // make a JSONObject containing all the crags, sectors, routes, grades ...
                        dictionary = new JSONObject(result);

                        // JSONObject -> ArrayList -> String[]
                        Iterator<String> iterCrags = dictionary.keys();
                        List<String> cragsList = new ArrayList<String>();

                        while(iterCrags.hasNext()) {
                            cragsList.add(iterCrags.next());
                        }

                        final String[] crags = cragsList.toArray(new String[cragsList.size()]);

                        // create adapter from array and fill the ListView
                        adapter = new ArrayAdapter<String>(getListView().getContext(), android.R.layout.simple_list_item_2, android.R.id.text1, crags) {

                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                                text1.setText(crags[position]);
                                try {
                                    text2.setText(dictionary.getJSONObject(crags[position]).getString("routes"));
                                } catch (JSONException e) {
                                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                                return view;
                            }
                        };

                        getListView().setAdapter(adapter);



                    } catch (JSONException e) {
                        Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        try {
            // get the name of the crag and the object for the crag
            String cragName = (String) adapter.getItem(position);

            String cragDictionary = dictionary.getString(cragName).toString();
            Intent intent = new Intent();

            intent.setClass(getApplicationContext(), cragDetails.class);

            Bundle bundle = new Bundle();

/*
cragDictionary = {"routes": 21, "sectors": { ... }}
*/
            bundle.putString("Vodnik.cragDictionary", cragDictionary);
            bundle.putString("Vodnik.cragName", cragName);

            intent.putExtras(bundle);
            startActivity(intent);

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_of_crags, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
