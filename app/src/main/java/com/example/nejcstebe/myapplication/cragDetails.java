package com.example.nejcstebe.myapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class cragDetails extends ListActivity {
    JSONObject cragDictionary;
    ArrayAdapter<String> adapter = null;
    String cragName = "";
    int numberOfRoutes = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crag_details);

        String result = "";
        Intent callingIntent = getIntent();

        if(callingIntent != null) {
            Bundle receivedBundle = callingIntent.getExtras();
            if(receivedBundle != null) {
                result = receivedBundle.getString("Vodnik.cragDictionary");
                if(result != null) {
                    try {
                        TextView tvCragName = (TextView) findViewById(R.id.textViewCrag);
                        TextView tvCragNumberOfRoutes = (TextView) findViewById(R.id.textViewNumber);

                        // make a JSONObject containing all the crags, sectors, routes, grades ...
                        cragDictionary = new JSONObject(result);

                        numberOfRoutes = cragDictionary.getInt("routes");
                        cragName = receivedBundle.getString("Vodnik.cragName");

                        tvCragName.setText(cragName);
                        tvCragNumberOfRoutes.setText("Število smeri: " + Integer.toString(numberOfRoutes));


                        JSONObject sectorsObj = cragDictionary.getJSONObject("sectors");

                        // JSONObject -> ArrayList -> String[]
                        Iterator<String> iterSectors = sectorsObj.keys();
                        List<String> sectorsList = new ArrayList<String>();

                        while(iterSectors.hasNext()) {
                            sectorsList.add(iterSectors.next());
                        }

                        String[] sectors = sectorsList.toArray(new String[sectorsList.size()]);

                        // create adapter from array and fill the ListView
                        adapter = new ArrayAdapter<String>(getListView().getContext(), android.R.layout.simple_list_item_1, sectors);
                        getListView().setAdapter(adapter);

                    } catch (JSONException e) {
                        Toast.makeText(getBaseContext(), "cragDetails: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        try {
            String sectorName = (String) adapter.getItem(position);
            String routesDictionary = cragDictionary.getJSONObject("sectors").getJSONObject(sectorName).toString();

            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), ListOfRoutes.class);
            Bundle bundle = new Bundle();

            bundle.putString("Vodnik.routesDictionary", routesDictionary);
            bundle.putString("Vodnik.cragName", cragName);
            bundle.putString("Vodnik.sectorName", sectorName);

            intent.putExtras(bundle);
            startActivity(intent);

            // start the crag activity
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crag_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
