package com.example.nejcstebe.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class RouteDetails extends Activity {
    String routeName, routeGrade, routeDesc;
    int routeLength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_details);

        String result = "";
        Intent callingIntent = getIntent();

        if(callingIntent != null) {
            Bundle receivedBundle = callingIntent.getExtras();
            if(receivedBundle != null) {

                TextView tvName = (TextView) findViewById(R.id.textViewRouteName);
                TextView tvGrade = (TextView) findViewById(R.id.textViewRouteGrade);
                TextView tvLength = (TextView) findViewById(R.id.textViewRouteLength);
                TextView tvDesc = (TextView) findViewById(R.id.textViewRouteDesc);

                routeName = receivedBundle.getString("Vodnik.routeName");
                routeGrade = receivedBundle.getString("Vodnik.routeGrade");
                routeDesc = receivedBundle.getString("Vodnik.routeDesc");
                routeLength = receivedBundle.getInt("Vodnik.routeLength");

                tvName.setText(routeName);
                tvGrade.setText("Ocena: " + routeGrade);
                tvLength.setText("Dolžina: " + Integer.toString(routeLength) + "m");
                tvDesc.setText("Opis smeri: " + routeDesc);
            }
        }
    }

    public void routeSent(View view) {
        FileOutputStream fOut = null;
        try {
            fOut = openFileOutput("ascents", MODE_APPEND);
        } catch(FileNotFoundException e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        String ascent = routeName + " " + routeGrade + " " + Integer.toString(routeLength) + "m";

        SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy");
        String currentDateAndTime = sdf.format(new Date());

        ascent += " " + currentDateAndTime + "\n";

        if (fOut != null) {
            try {
                fOut.write(ascent.getBytes());
                fOut.close();
                Toast.makeText(getBaseContext(), "Vzpon zabeležen", Toast.LENGTH_SHORT).show();
            } catch(IOException e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_route_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
