package com.example.nejcstebe.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openCrags(View view) {

        // check for internet connection
        if (!isConnected()) {
            Toast.makeText(getBaseContext(), "No internet connection", Toast.LENGTH_LONG).show();
        } else {
            String getCragsURL = "http://54.77.36.132:1337/object";
            new HttpAsyncTask().execute(getCragsURL);
        }
    }

    public void displayHelp(View view) {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), HelpActivity.class);
        startActivity(intent);
    }

    public void displayMyAscents(View view) {
        FileInputStream fin = null;
        try {
            fin = openFileInput("ascents");
            BufferedReader br = new BufferedReader(new InputStreamReader(fin));

            String nextLine;
            ArrayList<String> list = new ArrayList<String>();

            while ((nextLine = br.readLine()) != null) {
                list.add(nextLine);
            }

            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), myAscents.class);
            Bundle bundle = new Bundle();

            bundle.putStringArrayList("Vodnik.ascents", list);

            intent.putExtras(bundle);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fin != null) {
                try {
                    fin.close();
                } catch (IOException ignored) { }
            }
        }

    }


    // the task that gets the JSON object from the server
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            // Display success toast
            //Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();

            try {
                // give the new activity the string with the dictionary
                Intent intent = new Intent();

                intent.setClass(getApplicationContext(), ListOfCrags.class);

                Bundle bundle = new Bundle();

                bundle.putString("Vodnik.dictionary", result);

                intent.putExtras(bundle);
                startActivity(intent);

            } catch (Exception e) {
                Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    // execute GET request to the provided url and return (String) response
    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    // checks if we are connected to the internets
    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    // gets InputStream from HTTP returns a string (JSON)
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}
